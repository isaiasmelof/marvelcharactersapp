//
//  ListCharactersViewControllerTests.swift
//  MarvelCharactersAppTests
//
//  Created by Isaias Fernandes on 15/08/21.
//

import FBSnapshotTestCase
@testable import MarvelCharactersApp

class ListCharactersViewControllerTests: FBSnapshotTestCase {
    
    class ResponderMock: ListCharactersViewResponder {
        func listCharactersTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 2
        }
        
        func listCharactersTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = CharacterTableViewCell()
            
            return cell
        }
    }
    
    var sut: ListCharactersViewController!
    
    override func setUp() {
        super.setUp()
        recordMode = false
        let window = UIWindow.init()
        sut = .init()
        sut.setup(interactor: nil, router: nil)
        window.rootViewController = sut
        window.makeKeyAndVisible()
    }
    
    override func tearDown() {
        // Liberar memória para evitar excesso de alocações
        sut = nil
        super.tearDown()
    }
    
    func testViewController() {
        sut.view.backgroundColor = .red
        let characters = [Character(id: 1, name: "Alo", description: "teste", thumbnail: Thumbnail(path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784", thumbnailExtension: "jpg")), Character(id: 1, name: "Tchau", description: "testado", thumbnail: Thumbnail(path: "http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16", thumbnailExtension: "jpg"))]
        sut.displayHomeCharacters(viewModel: ListCharactersModels.ViewModel(offset: 0, limit: 0, total: 0, characters: characters, viewState: .showingCharactersList))
        FBSnapshotVerifyViewController(sut)
    }
    
    
//    func testViewController1() {
//        sut.displayHomeCharacters(viewModel: ListCharactersModels.ViewModel(characters: CharactersResponse(data: CharactersResult(offset: 0, limit: 0, total: 10, characters: [Character(id: 1, name: "Alo", description: "teste", thumbnail: Thumbnail(path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784", thumbnailExtension: "jpg")), Character(id: 1, name: "Tchau", description: "testado", thumbnail: Thumbnail(path: "http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16", thumbnailExtension: "jpg"))]))))
//        sut.view.backgroundColor = .red
//        FBSnapshotVerifyView(sut.view)
//    }
    
    
    
}

