//
//  CharacterRealmWorker.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 15/08/21.
//

import Foundation
import RealmSwift

protocol CharacterDataBaseServicing {
    
    func insert(request: ListCharactersModels.LocalDataBase.InsertRequest, completion: @escaping (Bool) -> Void)
    func fetchFavoritesCharacters(completion: @escaping ([CharacterEntity]) -> Void)
    func fetchFavoriteCharacter(request: ListCharactersModels.LocalDataBase.FetchFavoriteRequest, completion: @escaping (CharacterEntity?) -> Void)
    func delete(request: ListCharactersModels.LocalDataBase.DeleteRequest, completion: @escaping (Bool) -> Void)
}

class CharacterRealmWorker: CharacterDataBaseServicing {
    
    var notificationToken: NotificationToken?
    var realm: Realm!
    
    init() {
        self.realm = try! Realm()
    }
    
    func insert(request: ListCharactersModels.LocalDataBase.InsertRequest, completion: @escaping (Bool) -> Void) {
        
        do{
            try realm.write {
                realm.add(request.character)
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
    
    func fetchFavoritesCharacters(completion: @escaping ([CharacterEntity]) -> Void) {
        let characters = realm.objects(CharacterEntity.self)
        completion(Array(characters))
    }
    
    func delete(request: ListCharactersModels.LocalDataBase.DeleteRequest, completion: @escaping (Bool) -> Void) {
        do{
            try realm.write {
                realm.delete(realm.objects(CharacterEntity.self).filter("id == \(request.id)"))
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
    
    func fetchFavoriteCharacter(request: ListCharactersModels.LocalDataBase.FetchFavoriteRequest, completion: @escaping (CharacterEntity?) -> Void) {
        do{
            try realm.write {
                let result = realm.objects(CharacterEntity.self).filter("id == \(request.id)")
                guard let character = result.first else {
                    completion(nil)
                    return
                }
                completion(character)
            }
        } catch {
            completion(nil)
        }
    }
    
    
    deinit {
        notificationToken?.invalidate()
    }
}

