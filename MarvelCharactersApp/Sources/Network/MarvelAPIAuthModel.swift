//
//  MarvelAPIAuthModel.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import CryptoKit
import Foundation

struct MarvelAPIAuthModel {
    
    static let publicKey = "d9273ff06cce45ade0d7e61a0cbd524c"
    private static let privateKey = "21ce31d31c13b69304f31506d9f90e9f8a7ed827"
    static let timestamp = String(Date.timeIntervalBetween1970AndReferenceDate)
    
    static var hash: String? {
        let hashableString = "\(timestamp)\(privateKey)\(publicKey)"
        guard let hashableData = hashableString.data(using: .utf8) else { return "" }
        let hash = Insecure.MD5.hash(data: hashableData)
        return hash.map {
            String(format: "%02hhx", $0)
        }.joined()
    }
    
    private init () {}
    
    static var authString: String {
        return "?ts=\(timestamp)&apikey=\(publicKey)&hash=\(hash ?? "")"
    }
    
}
