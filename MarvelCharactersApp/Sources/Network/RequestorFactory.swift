//
//  RequestorFactory.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

final class RequestorFactory {
    
    func buildRequestor() -> NetworkRequesting {
        let urlBuider = URLBuilder()
        let requestBuilder = URLRequestBuilder()
        let requestor = NetworkRequestor(urlBuilder: urlBuider, requestBuilder: requestBuilder)
        return requestor
    }
}
