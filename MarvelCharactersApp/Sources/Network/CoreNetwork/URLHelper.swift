//
//  URLHelper.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

final class URLHelper {
    
    static func escapedParameters(_ parameters: [String: Any], shouldAddQuestionMark: Bool = true) -> String {
        if parameters.isEmpty {
            return .empty
        } else {
            var keyValuePairs = [String]()
            for (key, value) in parameters {
                let stringValue = "\(value)"
                guard let escapedValue = stringValue
                        .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                    continue
                }
                keyValuePairs.append(key + "=" + "\(escapedValue)")
            }
            let questionMarkText = shouldAddQuestionMark ? "?" : ""
            return questionMarkText + keyValuePairs.joined(separator: "&")
        }
    }
}
