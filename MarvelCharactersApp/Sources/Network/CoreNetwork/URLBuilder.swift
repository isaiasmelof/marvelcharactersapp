//
//  URLBuilder.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

enum BuilderError: Error {
    
    case missingValue
    case invalidValue
}

protocol URLBuildable {
    
    func set(baseURL: URL) -> Self
    func set(path: String) -> Self
    func set(parameters: NetworkRequestParameters?) -> Self
    func build() throws -> URL

}

final class URLBuilder: URLBuildable {
    
    private var baseURL: URL?
    private var path: String?
    private var queryItems: [URLQueryItem]?
    private var port: Int?
    
    @discardableResult
    func set(baseURL: URL) -> Self {
        self.baseURL = baseURL
        return self
    }
    
    @discardableResult
    func set(path: String) -> Self {
        self.path = path
        return self
    }
    
    @discardableResult
    func set(parameters: NetworkRequestParameters?) -> Self {
        guard let parameters = parameters, case let .queryItems(itemsTuple) = parameters else {
            return self
        }
        queryItems = itemsTuple.map({URLQueryItem(name: $0.key, value: $0.value)})
        return self
    }
    
    func build() throws -> URL {
        guard let baseURL = baseURL, let path = path else {
            throw BuilderError.missingValue
        }
        
        var urlComponent = URLComponents()
        urlComponent.scheme = baseURL.scheme
        urlComponent.port = baseURL.port
        urlComponent.host = baseURL.host
        urlComponent.path = path
        
        if let queryItems = queryItems {
            urlComponent.queryItems = queryItems
        }
        
        guard let url = urlComponent.url else {
            throw BuilderError.invalidValue
        }
        
        self.path = nil
        self.queryItems = nil
        return url
    }
    
}
