//
//  NetworkRequestor.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

protocol NetworkRequesting: AnyObject {
    
    func execute<ResponseType: Decodable>(responseType: ResponseType.Type, request: NetworkRequestBuilding, completion: @escaping (_ response: Result<ResponseType, NetworkError>) -> Void)
    func execute(request: NetworkRequestBuilding, completion: @escaping (_ response: Result<Data, NetworkError>) -> Void)
    func buildRequest(from request: NetworkRequestBuilding) throws -> URLRequest
}

protocol URLSessionable {
    
    func finishTasksAndInvalidate()
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

extension URLSession: URLSessionable {}

final class NetworkRequestor: NetworkRequesting {
    
    private var session: URLSessionable
    private var urlBuilder: URLBuildable
    private var requestBuilder: URLRequestBuilding
    
    init(
        session: URLSessionable = URLSession.shared,
        urlBuilder: URLBuildable,
        requestBuilder: URLRequestBuilding
    ) {
        self.session = session
        self.urlBuilder = urlBuilder
        self.requestBuilder = requestBuilder
    }
    
    func execute<ResponseType: Decodable>(responseType: ResponseType.Type, request: NetworkRequestBuilding, completion: @escaping (_ response: Result<ResponseType, NetworkError>) -> Void) {
        do{
            let urlRequest = try buildRequest(from: request)
            let task = session.dataTask(with: urlRequest) {[weak self](data, response, error) in
                self?.session.finishTasksAndInvalidate()
                guard let data = data, error == nil else {
                    completion(.failure(.requestFailure))
                    return
                }
                
                if let err = self?.containError(in: response) {
                    completion(.failure(err))
                    return
                }
                if let response = try? JSONDecoder().decode(ResponseType.self, from: data) {
                    completion(.success(response))
                }else{
                    completion(.failure(.decodingError))
                }
            }
            task.resume()
        } catch {
            completion(.failure(.buildRequestError))
        }
    }
    
    func execute(request: NetworkRequestBuilding, completion: @escaping (Result<Data, NetworkError>) -> Void) {
        do{
            let urlRequest = try buildRequest(from: request)
            let task = session.dataTask(with: urlRequest) {[weak self](data, response, error) in
                self?.session.finishTasksAndInvalidate()
                guard let data = data, error == nil else {
                    completion(.failure(.requestFailure))
                    return
                }
                if let err = self?.containError(in: response) {
                    completion(.failure(err))
                    return
                }
                completion(.success(data))
            }
            task.resume()
        } catch {
            completion(.failure(.buildRequestError))
        }
    }
    
    func buildRequest(from request: NetworkRequestBuilding) throws -> URLRequest {
        
        guard let baseURL = request.baseUrl else {
            throw BuilderError.invalidValue
        }
        
        let url = try urlBuilder
            .set(baseURL: baseURL)
            .set(path: request.path)
            .set(parameters: request.parameters)
            .build()
        
        let urlRequest = try requestBuilder
            .set(url: url)
            .set(headers: request.headers)
            .set(parameters: request.parameters)
            .set(method: request.method)
            .build()
        
        let configuration: URLSessionConfiguration = .ephemeral
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        session = URLSession.init(configuration: configuration)
        return urlRequest
    }
    
    private func containError(in httpResponse: URLResponse?) -> NetworkError? {
        guard let requestResponse = httpResponse as? HTTPURLResponse else {
            return .genericError
        }
        
        let successStatusRange = 200...299
        
        if !(successStatusRange ~= requestResponse.statusCode) {
            return .genericError
        }
        return nil
    }
    
}
