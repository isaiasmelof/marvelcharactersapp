//
//  NetworkErrors.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

enum NetworkError: Error {
    
    case decodingError
    case buildRequestError
    case requestFailure
    case genericError
}
