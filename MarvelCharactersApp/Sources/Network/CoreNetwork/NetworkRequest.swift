//
//  NetworkRequest.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

enum HTTPMethod: String {
    
    case get
    case put
    case post
    case delete
    
    var name: String {
        return self.rawValue.uppercased()
    }
}

protocol NetworkRequestBuilding {
    
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: NetworkRequestParameters? { get }
    var headers: [String: Any]? {get}
    var baseUrl: URL? {get}
    func timeout() -> TimeInterval?
}

extension NetworkRequestBuilding {
    
    var baseUrl: URL? {
        return URL(string: "https://gateway.marvel.com/")
    }
    
    func timeout() -> TimeInterval? {
        return nil
    }
}

public enum NetworkRequestParameters {
    
    case body(_: [String: Any]?)
    case url(_: [String: Any]?)
    case queryItems(items: [(key: String, value: String)])
    case json(model: Encodable)
    
    
    var contentType: String? {
        switch self {
        case .json:
            return "application/json"
        case .url:
            return "application/x-www-form-urlencoded"
        default:
            return nil
        }
    }
    
    var rawData: Data? {
        
        switch self {
        case .json(let encodableModel):
            return encodableModel.convertToData()
        case .url(let dictionary):
            guard let dictionary = dictionary else { return nil }
            return URLHelper.escapedParameters(dictionary, shouldAddQuestionMark: false).data(using: .utf8)
        case .body(let body):
            guard let body = body else { return nil }
            return try? JSONSerialization.data(withJSONObject: body, options: [])
        default:
            return nil
        }
    }
}
