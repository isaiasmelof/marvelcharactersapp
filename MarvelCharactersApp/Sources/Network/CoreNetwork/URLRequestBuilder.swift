//
//  URLRequestBuilder.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

protocol URLRequestBuilding {
    
    func set(url: URL) -> Self
    func set(method: HTTPMethod) -> Self
    func set(headers: [String: Any]?) -> Self
    func set(parameters: NetworkRequestParameters?) -> Self
    func build() throws -> URLRequest
}

final class URLRequestBuilder: URLRequestBuilding {
    
    private var url: URL?
    private var method: HTTPMethod = .get
    private var headers: [String: Any]?
    private var data: Data?
    private var contentType: String?
    
    @discardableResult
    func set(url: URL) -> Self {
        self.url = url
        return self
    }
    
    @discardableResult
    func set(method: HTTPMethod) -> Self {
        self.method = method
        return self
    }
    
    @discardableResult
    func set(headers: [String : Any]?) -> Self {
        self.headers = headers
        return self
    }
    
    @discardableResult
    func set(parameters: NetworkRequestParameters?) -> Self {
        guard let parameters = parameters else {
            return self
        }
        self.data = parameters.rawData
        guard let contentType = parameters.contentType else {
            return self
        }
        self.contentType = contentType
        return self
    }
    
    func build() throws -> URLRequest {
        guard let url = url else {
            throw BuilderError.invalidValue
        }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData)
        if let headers = headers {
            headers.forEach {
                request.addValue(String(describing: $0.value), forHTTPHeaderField: $0.key)
            }
        }
        if let contentType = contentType {
            request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        }
        request.httpMethod = method.rawValue
        request.httpBody = data
        
        self.headers = nil
        self.data = nil
        self.contentType = nil
        
        return request
    }
}
