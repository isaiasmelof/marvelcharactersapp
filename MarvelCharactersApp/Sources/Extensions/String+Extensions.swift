//
//  String+Extensions.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

extension String {
    
    static let empty = ""
}

extension String {
    
    var localizableString: String {
        return NSLocalizedString(self, tableName: "AppLocalizable", bundle: Bundle.main, value: "**\(self)**", comment: "")
    }
}
