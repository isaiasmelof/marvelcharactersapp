//
//  Int+Extensions.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation

extension Int {
    
    func toString() -> String {
        return String(self)
    }
}
