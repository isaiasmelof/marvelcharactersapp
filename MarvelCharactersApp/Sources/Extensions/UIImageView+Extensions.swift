//
//  UIImageView+Extensions.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

extension UIImageView {
    
    func loadImage(at url: URL) {
      UIImageLoader.loader.load(url, for: self)
    }

    func cancelImageLoad() {
      UIImageLoader.loader.cancel(for: self)
    }
}
