//
//  UIImage+Extensions.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

extension UIImage {
    
    class var characterThumbnailPlaceholder: UIImage {
        return UIImage(named: "character-thumbnail-placeholder", in: nil, compatibleWith: nil) ?? UIImage()
    }
    
    class var emptyContentPlaceholder: UIImage {
        return UIImage(named: "empty_feedback", in: nil, compatibleWith: nil) ?? UIImage()
    }
    
    class var networkErrorPlaceholder: UIImage {
        return UIImage(named: "request_failed", in: nil, compatibleWith: nil) ?? UIImage()
    }
    
    class var filledStar: UIImage {
        return UIImage(named: "filled_star", in: nil, compatibleWith: nil) ?? UIImage()
    }
    
    class var emptyStar: UIImage {
        return UIImage(named: "empty_star", in: nil, compatibleWith: nil) ?? UIImage()
    }
    
    class var homeIcon: UIImage {
        return UIImage(named: "home_icon", in: nil, compatibleWith: nil) ?? UIImage()
    }
    
    class var favIcon: UIImage {
        return UIImage(named: "fav_icon", in: nil, compatibleWith: nil) ?? UIImage()
    }
}
