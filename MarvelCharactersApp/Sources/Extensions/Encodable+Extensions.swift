//
//  Encodable+Extensions.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

extension Encodable {
    
    var dictionary: [String: Any]? {
        guard let data  = try? JSONEncoder().encode(self) else { return nil}
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap {
            $0 as? [String: Any]
        }
    }
    
    func convertToData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
    
    func toKeyValueArray() -> [(key: String, value: String)] {
        var result = [(key: String, value: String)]()
        for item in self.dictionary ?? [:] {
            result.append((key: item.key, value: String(describing: item.value)))
        }
        return result
    }
}
