//
//  BaseViewController.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

public protocol CodableView: class {
    
    func buildViews()
    func setupComponents()
    func setupConstraints()
}

open class BaseViewController<V: CodableView>: UIViewController where V: UIView {
    
    
    public var associatedView: V! { return self.view as? V }
    
    override open func loadView() {
        
        self.view = V()
        
        self.associatedView.setupComponents()
        self.associatedView.buildViews()
        self.associatedView.setupConstraints()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
