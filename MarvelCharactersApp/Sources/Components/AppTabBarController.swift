//
//  AppTabBarController.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 18/08/21.
//

import Foundation
import UIKit

final class AppTabBarController: UITabBarController {
    
    init(firstViewController: UINavigationController, secondViewController: UINavigationController) {
        super.init(nibName: nil, bundle: nil)
        viewControllers = [firstViewController, secondViewController]
        setup(firstViewController, secondViewController)
        tabBar.isTranslucent = false
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup(_ firstViewController: UINavigationController, _ secondViewController: UINavigationController) {
        firstViewController.navigationBar.isTranslucent = false
        secondViewController.navigationBar.isTranslucent = false
        let item1 = UITabBarItem(title: "home".localizableString, image: .homeIcon, tag: .zero)
        let item2 =  UITabBarItem(title: "favorites".localizableString, image: .favIcon, tag: 1)
        firstViewController.tabBarItem = item1
        secondViewController.tabBarItem = item2
    }
    
}
