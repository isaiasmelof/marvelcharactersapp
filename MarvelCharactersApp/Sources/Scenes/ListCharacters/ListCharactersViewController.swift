//
//  ListCharactersViewController.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

protocol ListCharactersDisplayLogic: AnyObject {
    
    func displayListCharacters(viewModel: ListCharactersModels.ViewModel)
    func displayFavoriteOperationsFeedback(viewModel:  ListCharactersModels.FavoritedOperationsViewModel)
    func displayCharacterDetails(viewModel:  ListCharactersModels.DetailCharacterViewModel)
}

class ListCharactersViewController: BaseViewController<ListCharactersView>, ListCharactersDisplayLogic {

    // MARK: - Internal Properties
    let requestor = RequestorFactory().buildRequestor()
    
    // MARK: - Private Properties
    
    private var interactor: ListCharactersBusinessLogic?
    private var displayedCharacters: [Character] = []
    private var filteredCharacters: [Character] = []
    
    // MARK: - Internal Properties
    
    var router: ListsCharactersRoutingLogic?
    
    var selectedCharactersTableViewIndex: Int? {
        return associatedView.selectedCharactersTableViewIndex()
    }
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemBackground
        title = "characters_list_view_controller_title".localizableString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.requestedCharacter()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.associatedView.resetSearchBarText()
        interactor?.resquestedResetFavoriteButtons()
    }
    
    // MARK: - HomeDisplayLogic Methods
    
    func displayListCharacters(viewModel: ListCharactersModels.ViewModel) {
        displayedCharacters = viewModel.characters ?? []
        filteredCharacters =  viewModel.characters ?? []
        DispatchQueue.main.async { [weak self] in
            self?.associatedView.setupFrom(viewModel: viewModel)
        }
    }
    
    func displayFavoriteOperationsFeedback(viewModel:  ListCharactersModels.FavoritedOperationsViewModel) {
        showToast(message: viewModel.feedbackMessage, font: viewModel.font)
    }
      
    func displayCharacterDetails(viewModel: ListCharactersModels.DetailCharacterViewModel) {
        router?.routeToDetailCharacter(character: viewModel.character)
    }

    // MARK: - Helpers
    
    func setup(interactor: ListCharactersInteractor?, router: ListsCharactersRouter?) {
        self.interactor = interactor
        self.router = router
        associatedView.setupViewDelegates(delegate: self, dataSource: self, searchBarDelegate: self, viewDelegate: self)
    }

}

extension ListCharactersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCharacters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reusableCell = tableView.dequeueReusableCell(withIdentifier: CharacterTableViewCell.className, for: indexPath)
        
        guard let cell = reusableCell as? CharacterTableViewCell,
              let character = filteredCharacters[safe: indexPath.row] else {
            return CharacterTableViewCell()
        }
        
        cell.setup(
            viewModel: ListCharactersModels.ViewModel.CharacterCellViewModel(
                thumbnailUrl: character.thumbnail.urlToDownload,
                characterName: character.name,
                isFavorited: character.isFavorited ?? false,
                delegate: self)
        )
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor?.requestedDetailCharacter(indexCharacter: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == displayedCharacters.count  - 1 else { return }
        interactor?.requestedCharactersPagination(currentIndex: indexPath.row)
    }
}

extension ListCharactersViewController: CharacterTableViewCellDelegate {
    
    func characterTableViewCellDidTapFavoriteButton(_ view: CharacterTableViewCell, isFavorited: Bool) {
        guard let index = associatedView.indexFor(cell: view) else { return }
        interactor?.requestedFavoriteCharacter(isFavorited: isFavorited, indexCharacter: index)
    }
}

extension ListCharactersViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        interactor?.requestedFilteredCharacters(searchText: searchText)
    }
    
}

extension ListCharactersViewController: ListCharactersViewDelegate {
    
    func listCharactersViewDidTapFeedBackViewButton(_ view: ListCharactersView) {
        interactor?.requestedCharacter()
    }
}

