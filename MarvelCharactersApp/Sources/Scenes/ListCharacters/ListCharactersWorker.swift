//
//  ListCharactersWorker.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation

protocol ListCharactersWorkable: AnyObject {
    
    func fetchListCharactersPagination(request: ListCharactersModels.PaginationRequest, completion: @escaping (Result<ListCharactersModels.Response, NetworkError>) -> Void)
    func fetchListCharacters(completion: @escaping (Result<ListCharactersModels.Response, NetworkError>) -> Void)
    func insertFavoriteCharacter(request: ListCharactersModels.LocalDataBase.InsertRequest, completion: @escaping (Bool) -> Void)
    func fetchFavoriteCharacter(request: ListCharactersModels.LocalDataBase.FetchFavoriteRequest, completion: @escaping (CharacterEntity?) -> Void)
    func fetchAllFavoriteCharacter(completion: @escaping ([CharacterEntity]) -> Void)
    func deleteFavorite(request: ListCharactersModels.LocalDataBase.DeleteRequest, completion: @escaping (Bool) -> Void)

}

final class ListCharactersWorker: ListCharactersWorkable {
    
    // MARK: - Private variables
    
    private var networkRequestor: NetworkRequesting
    private var realmService: CharacterDataBaseServicing
    
    // MARK: - Initializer
    
    init(networkRequestor: NetworkRequesting, realmService: CharacterDataBaseServicing) {
        self.realmService = realmService
        self.networkRequestor = networkRequestor
    }
    
    // MARK: - ListCharactersWorkable methods
    
    func fetchListCharacters(completion: @escaping (Result<ListCharactersModels.Response, NetworkError>) -> Void) {
        
        let httpRequest: MarvelCharactersHttpRequest = .characters(CharactersRequestContent())
        
        networkRequestor.execute(responseType: CharactersResponse.self, request: httpRequest) { (result) in
            switch result {
            case let .success(response):
                completion(.success(ListCharactersModels.Response(characters: response.data, error: nil)))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    func fetchListCharactersPagination(request: ListCharactersModels.PaginationRequest, completion: @escaping (Result<ListCharactersModels.Response, NetworkError>) -> Void) {
        
        let httpRequest: MarvelCharactersHttpRequest = .characters(CharactersRequestContent(offset: request.offset.toString()))
        networkRequestor.execute(responseType: CharactersResponse.self, request: httpRequest) { (result) in
            switch result {
            case let .success(response):
                completion(.success(ListCharactersModels.Response(characters: response.data, error: nil)))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    func insertFavoriteCharacter(request: ListCharactersModels.LocalDataBase.InsertRequest, completion: @escaping (Bool) -> Void) {
        realmService.insert(request: request) { (sucess) in
            completion(sucess)
        }
    }
    
    func fetchFavoriteCharacter(request: ListCharactersModels.LocalDataBase.FetchFavoriteRequest, completion: @escaping (CharacterEntity?) -> Void) {
        realmService.fetchFavoriteCharacter(request: request) { (character) in
            completion(character)
        }
    }
    
    func fetchAllFavoriteCharacter(completion: @escaping ([CharacterEntity]) -> Void) {
        realmService.fetchFavoritesCharacters { (allCharacters) in
            completion(allCharacters)
        }
    }
    
    func deleteFavorite(request: ListCharactersModels.LocalDataBase.DeleteRequest, completion: @escaping (Bool) -> Void) {
        realmService.delete(request: request) { (sucess) in
            completion(sucess)
        }
    }
}
