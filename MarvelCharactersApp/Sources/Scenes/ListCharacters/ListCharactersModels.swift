//
//  ListCharactersModels.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

struct ListCharactersModels {
    
    struct LocalDataBase {
        
        struct InsertRequest {
            let character: CharacterEntity
        }
        
        struct FetchFavoriteRequest {
            let id: Int
        }
        
        struct DeleteRequest {
            let id: Int
        }
    }
    
    struct FavoritedOperationsResponse {
        let feedbackMessage: String
    }
    
    struct DetailCharacterResponse {
        let character: Character
    }
    
    struct Response {
        let characters: CharactersResult?
        let error: NetworkError?
    }
    
    struct PaginationRequest {
        let offset: Int
    }
    
    struct RefreshCharacterImageViewModel {
        let characterImage: UIImage
        let currentIndex: Int
        let total: Int
    }
    
    struct FavoritedOperationsViewModel {
        let feedbackMessage: String
        let font: UIFont = .boldSystemFont(ofSize: 12)
    }
    
    struct DetailCharacterViewModel {
        let character: Character
    }
    
    struct ViewModel {
        
        enum State {
            case fetchingCharactersLoading
            case fetchingCharactersLoadingFinished
            case showingCharactersList
            case showingFeedback
            case charactersListPagingLoading
        }
        
        let offset: Int?
        let limit: Int?
        let total: Int?
        let characters: [Character]?
        let feedbackImage: UIImage?
        let feedbackMessage: String?
        let feedbackTitleButton: String?
        let viewState: State
        
        init(
            offset: Int? = nil,
            limit: Int? = nil,
            total: Int? = nil,
            characters: [Character]? = nil,
            feedbackImage: UIImage? = nil,
            feedbackMessage: String? = nil,
            feedbackTitleButton: String? = nil,
            viewState: State
        ) {
            self.offset = offset
            self.limit = limit
            self.total = total
            self.characters = characters
            self.feedbackImage = feedbackImage
            self.feedbackMessage = feedbackMessage
            self.feedbackTitleButton = feedbackTitleButton
            self.viewState = viewState
        }
        
        struct CharacterCellViewModel {
            
            let thumbnailUrl: String
            let characterName: String
            let isFavorited: Bool
            let delegate: CharacterTableViewCellDelegate?
            let isFavoriteButtonHidden: Bool
            
            init(
                thumbnailUrl: String,
                characterName: String,
                isFavorited: Bool = false,
                delegate: CharacterTableViewCellDelegate? = nil,
                isFavoriteButtonHidden: Bool = false
            ) {
                self.thumbnailUrl = thumbnailUrl
                self.characterName = characterName
                self.isFavorited = isFavorited
                self.delegate = delegate
                self.isFavoriteButtonHidden = isFavoriteButtonHidden
            }
        }
    }
}
