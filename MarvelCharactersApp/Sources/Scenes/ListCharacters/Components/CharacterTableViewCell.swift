//
//  CharacterTableViewCell.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

protocol CharacterTableViewCellDisplayLogic {
    
    func setup(viewModel: ListCharactersModels.ViewModel.CharacterCellViewModel)
}

protocol CharacterTableViewCellDelegate: AnyObject {
    
    func characterTableViewCellDidTapFavoriteButton(_ view: CharacterTableViewCell, isFavorited: Bool)
}

final class CharacterTableViewCell: UITableViewCell, CodableView, CharacterTableViewCellDisplayLogic {
    
    // MARK: - Constants
    
    enum Constants {
        
        struct CharacterImageView {
            static let size: CGFloat = 40
            static let leading: CGFloat = 16
            static let verticalMargin: CGFloat = 8
            static let cornerRadius: CGFloat = size / 2
        }
        
        struct FavoriteButton {
            static let size: CGFloat = 25
            static let trailing: CGFloat = 8
        }
        
        struct ContentStackView {
            static let spacing: CGFloat = 4
            static let top: CGFloat = 8
        }
        
        struct CharacterImageViewAndNameLabelStackView {
            static let spacing: CGFloat = 8
        }
        
        struct SepratorView {
            static let height: CGFloat = 1
        }
    }
    
    var onReuse: () -> Void = {}
    
    override func prepareForReuse() {
        super.prepareForReuse()
        characterImageView.image = nil
    }
    
    // MARK: - UI Components
    
    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [characterImageViewAndNameLabelStackView, wrapperSeparatorView])
        stack.axis = .vertical
        stack.spacing = Constants.ContentStackView.spacing
        return stack
    }()
    
    private lazy var characterImageViewAndNameLabelStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [wrapperCharacterImageView, characterNameLabel, wrapperFavoriteButtom])
        stack.axis = .horizontal
        stack.spacing = Constants.CharacterImageViewAndNameLabelStackView.spacing
        return stack
    }()
    
    private lazy var wrapperCharacterImageView: UIView = {
        let view = UIView()
        view.setContentHuggingPriority(.required, for: .horizontal)
        return view
    }()
    
    private lazy var wrapperFavoriteButtom: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var characterImageView: CachedImageView = {
        let imageView = CachedImageView(cornerRadius: Constants.CharacterImageView.cornerRadius, emptyImage: .characterThumbnailPlaceholder)
        imageView.shouldUseEmptyImage = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var characterNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = .zero
        return label
    }()
    
    private lazy var wrapperSeparatorView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .separator
        return view
    }()
    
    private lazy var favoriteButton: UIButton = {
        let button = UIButton()
        button.setImage(.emptyStar, for: .normal)
        button.addTarget(self, action: #selector(didTapFavButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Internal Properties
    
    weak var delegate: CharacterTableViewCellDelegate?

    // MARK: - Initializers
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - CodableView Methods
    
    func buildViews() {
        contentView.addSubview(contentStackView)
        wrapperFavoriteButtom.addSubview(favoriteButton)
        wrapperSeparatorView.addSubview(separatorView)
        wrapperCharacterImageView.addSubview(characterImageView)
    }
    
    func setupComponents() {
        
    }
    
    func setupConstraints() {
        contentStackView.snp.makeConstraints { (make) in
            make.top.equalTo(Constants.ContentStackView.top)
            make.bottom.left.right.equalToSuperview()
        }
        
        characterImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(Constants.CharacterImageView.size)
            make.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().inset(Constants.CharacterImageView.leading)
        }
        
        separatorView.snp.makeConstraints { (make) in
            make.height.equalTo(Constants.SepratorView.height)
            make.top.trailing.equalToSuperview().inset(Constants.CharacterImageView.verticalMargin)
            make.leading.equalToSuperview().inset(Constants.CharacterImageView.leading)
            make.bottom.equalToSuperview()
        }
        
        wrapperFavoriteButtom.snp.makeConstraints { (make) in
            make.width.lessThanOrEqualTo(41)
        }
        
        favoriteButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(Constants.FavoriteButton.trailing)
            make.height.width.equalTo(Constants.FavoriteButton.size)
        }
    }
    
    // MARK: - CharacterTableViewCellDisplayLogic Methods
    
    func setup(viewModel: ListCharactersModels.ViewModel.CharacterCellViewModel) {
        buildViews()
        setupComponents()
        setupConstraints()
        characterImageView.loadImage(urlString: viewModel.thumbnailUrl)
        characterNameLabel.text = viewModel.characterName
        let favoriteButtonImage: UIImage = viewModel.isFavorited ? .filledStar : .emptyStar
        favoriteButton.setImage(favoriteButtonImage, for: .normal)
        delegate = viewModel.delegate
        setupFavoriteButtonVisibility(isHidden: viewModel.isFavoriteButtonHidden)
    }
    
    private func setupFavoriteButtonVisibility(isHidden: Bool) {
        favoriteButton.isHidden = isHidden
        favoriteButton.isUserInteractionEnabled = !isHidden
    }
    
    // MARK: - Private Methods
    
    @objc
    private func didTapFavButton() {
        let newButtonImage: UIImage = favoriteButton.imageView?.image == UIImage.emptyStar ? .filledStar : .emptyStar
        let isFavorited = newButtonImage == .filledStar
        favoriteButton.setImage(newButtonImage, for: .normal)
        delegate?.characterTableViewCellDidTapFavoriteButton(self, isFavorited: isFavorited)
    }
}
