//
//  CachedImageView.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 15/08/21.
//

import Foundation
import UIKit

/**
    Esta implementação foi extraída de projeto de terceiros com objetivo de abstrair a lógica de cachear imagens usando NSCache.
    Projeto Original pode ser encontrado em: https://www.letsbuildthatapp.com/course_video?id=972
 */

class CachedImageView: UIImageView {
    
    static let imageCache = NSCache<NSString, DiscardableImageCacheItem>()
    
    var shouldUseEmptyImage = true
    
    private var urlStringForChecking: String?
    private var emptyImage: UIImage?
    
    convenience init(cornerRadius: CGFloat = 0, tapCallback: @escaping (() ->())) {
        self.init(cornerRadius: cornerRadius, emptyImage: nil)
        self.tapCallback = tapCallback
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
    
    @objc func handleTap() {
        tapCallback?()
    }
    
    var tapCallback: (() -> ())?
    
     init(cornerRadius: CGFloat = 0, emptyImage: UIImage? = nil) {
        super.init(frame: .zero)
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        self.emptyImage = emptyImage
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func loadImage(urlString: String, completion: (() -> ())? = nil) {
        image = nil
        
        self.urlStringForChecking = urlString
        
        let urlKey = urlString as NSString
        
        if let cachedItem = CachedImageView.imageCache.object(forKey: urlKey) {
            image = cachedItem.image
            completion?()
            return
        }
        
        runDownloadImageTask(urlString: urlString, completion: completion)

    }
    
    private func runDownloadImageTask(urlString: String, completion: (() -> ())? = nil) {
        guard let urlObj = URL(string: urlString) else {
            if shouldUseEmptyImage {
                image = emptyImage
            }
            return
        }
        image = emptyImage
        let urlKey = urlString as NSString
        URLSession.shared.dataTask(with: urlObj, completionHandler: { [weak self] (data, response, error) in
            if error != nil {
                return
            }
            guard let data = data else { return }
            DispatchQueue.main.async {
                if let image = UIImage(data: data) {
                    let cacheItem = DiscardableImageCacheItem(image: image)
                    CachedImageView.imageCache.setObject(cacheItem, forKey: urlKey)
                    
                    if urlString == self?.urlStringForChecking {
                        self?.image = image
                        completion?()
                    }
                }
            }
            
            }).resume()
    }
}

class DiscardableImageCacheItem: NSObject, NSDiscardableContent {
    
    private(set) public var image: UIImage?
    var accessCount: UInt = 0
    
    init(image: UIImage) {
        self.image = image
    }
    
    func beginContentAccess() -> Bool {
        if image == nil {
            return false
        }
        
        accessCount += 1
        return true
    }
    
    func endContentAccess() {
        if accessCount > 0 {
            accessCount -= 1
        }
    }
    
    func discardContentIfPossible() {
        if accessCount == 0 {
            image = nil
        }
    }
    
    func isContentDiscarded() -> Bool {
        return image == nil
    }
    
}
