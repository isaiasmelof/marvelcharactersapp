//
//  FeedbackView.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 15/08/21.
//

import Foundation
import UIKit

protocol FeedbackViewDelegate: AnyObject {
    
    func feedbackViewDidTapFeedbackButton(_ view: FeedbackView)
}

final class FeedbackView: UIStackView {
    
    // MARK: - Constants
    
    enum Constants {
        
        static let spacing: CGFloat = 16
        
        struct ImageView {
            static let cornerRadius: CGFloat = 8
            static let size: CGFloat = 200
            static let defautMargin: CGFloat = 8
        }
        
        struct ActionButton {
            static let cornerRadius: CGFloat = 8
            static let height: CGFloat = 44
            static let horizontalMargin: CGFloat = 24
            static let titleFontSize: CGFloat = 16
        }
    }
    
    // MARK: Internal Properties
    
    weak var delegate: FeedbackViewDelegate?
    
    // MARK: - UI Components
    
    private lazy var wrapperMessageImageView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var feedbackMessageImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        
        return imageView
    }()
    
    private lazy var wrapperFeedbackMessageLabel: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var feedbackMessageLabel: UILabel = {
        let label = UILabel()
        label.setContentHuggingPriority(.required, for: .vertical)
        label.numberOfLines = .zero
        label.textAlignment = .center
        return label
    }()
    
    private lazy var wrapperFeedbackActionButton: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var feedActionButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = Constants.ActionButton.cornerRadius
        button.backgroundColor = .systemRed
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: Constants.ActionButton.titleFontSize)
        button.addTarget(self, action: #selector(didTapFeedbackActionButton), for: .touchUpInside)
        return button
    }()

    // MARK: - Initializers
    
    init() {
        super.init(frame: .zero)
    }
    
    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - CodableView Methods
    
    func buildViews() {
        addArrangedSubview(wrapperMessageImageView)
        addArrangedSubview(wrapperFeedbackMessageLabel)
        addArrangedSubview(wrapperFeedbackActionButton)
        wrapperFeedbackMessageLabel.addSubview(feedbackMessageLabel)
        wrapperFeedbackActionButton.addSubview(feedActionButton)
        wrapperMessageImageView.addSubview(feedbackMessageImageView)
    }
    
    func setupComponents() {
        axis = .vertical
        spacing = Constants.spacing
        alignment = .center
    }
    
    func setupConstraints() {
        
        feedbackMessageImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(Constants.ImageView.size)
            make.top.bottom.leading.trailing.equalToSuperview().inset(Constants.ImageView.defautMargin)
        }
        
        wrapperFeedbackActionButton.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
        }
        
        feedActionButton.snp.makeConstraints { (make) in
            make.height.equalTo(Constants.ActionButton.height)
            make.top.bottom.equalToSuperview().inset(Constants.ImageView.defautMargin)
            make.leading.trailing.equalToSuperview().inset(Constants.ActionButton.horizontalMargin)
        }
        
        feedbackMessageLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(Constants.ActionButton.horizontalMargin)
            make.top.bottom.equalToSuperview()
        }
    }
    
    // MARK: - Private Methods
    
    func setup(feedbackImage: UIImage, feedbackMessage: String, feedbackActionButtonTitle: String?){
        buildViews()
        setupComponents()
        setupConstraints()
        feedbackMessageImageView.image = feedbackImage
        feedbackMessageLabel.text = feedbackMessage
        guard let title = feedbackActionButtonTitle else {
            feedActionButton.isHidden = true
            return
        }
        feedActionButton.isHidden = false
        feedActionButton.setTitle(title, for: .normal)
        
    }
    
    @objc
    private func didTapFeedbackActionButton() {
        delegate?.feedbackViewDidTapFeedbackButton(self)
    }
}
