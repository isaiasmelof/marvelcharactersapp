//
//  ListsCharactersRouter.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation

protocol ListsCharactersRoutingLogic {
    
    func routeToDetailCharacter(character: Character)
}

protocol ListsCharactersDataPassing {
    
    var dataStore: ListCharactersDataStore? { get }
}

final class ListsCharactersRouter: NSObject, ListsCharactersRoutingLogic {
    
    // MARK:  - Private properties
    
    private weak var viewController: ListCharactersViewController?
    
    // MARK:  - Internal properties
    var dataStore: ListCharactersDataStore?
    
    // MARK:  - Initializer
    
    init(viewController: ListCharactersViewController, dataStore: ListCharactersDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }
    
    // MARK:  - ListsCharactersRoutingLogic Methods
    
    func routeToDetailCharacter(character: Character) {
        let nextViewController = CharacterDetailFactory().setupCharacterDetail(character: character, realmService: dataStore?.realmService)
        viewController?.navigationController?.pushViewController(nextViewController, animated: true)
    }
}
