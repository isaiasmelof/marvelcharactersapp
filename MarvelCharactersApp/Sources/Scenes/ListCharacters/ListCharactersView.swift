//
//  ListCharactersView.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

protocol ListCharactersViewDelegate: AnyObject {
    
    func listCharactersViewDidTapFeedBackViewButton(_ view: ListCharactersView)
}

final class ListCharactersView: UIView, CodableView {
    
    // MARK: - Constants properties
    
    enum Constants {
        
        struct LoadingView {
            static let activityIndicatorColor = UIColor.systemRed
            static let backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        
        struct FeedbackView {
            static let heightMultiplerScale: CGFloat = 0.70
            static let top: CGFloat = 32
        }
    }
    
    // MARK: - Internal properties
    
    weak var delegate: ListCharactersViewDelegate?
    
    // MARK: - UI Components
    
    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [charactersTableView, wrapperFeedbackView])
        return stack
    }()
    
    private lazy var charactersTableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.separatorStyle = .none
        return tableView
    }()
    
    private lazy var loading: UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView(style: .medium)
        loading.color = .systemBlue
        loading.backgroundColor = Constants.LoadingView.backgroundColor
        return loading
    }()
    
    
    private lazy var wrapperFeedbackView: UIView = {
        let view = UIView()
        view.isHidden = true
        return view
    }()
    
    private lazy var feedbackView: FeedbackView = {
        let view = FeedbackView()
        view.delegate = self
        return view
    }()
    
    private var searchBar: UISearchBar = {
        let search = UISearchBar()
        search.barStyle = .default
        search.sizeToFit()
        search.isTranslucent = false
        search.backgroundImage = UIImage()
        
        return search
    }()
    
    
    // MARK: - Initializers
    
    init() {
        super.init(frame: .zero)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - CodableView Methods
    
    func buildViews() {
        addSubview(contentStackView)
        addSubview(loading)
        wrapperFeedbackView.addSubview(feedbackView)
    }
    
    func setupComponents() {
        backgroundColor = .systemBackground
        contentStackView.axis = .vertical
        charactersTableView.tableHeaderView = searchBar
        charactersTableView.register(CharacterTableViewCell.self, forCellReuseIdentifier: CharacterTableViewCell.className)
    }
    
    func setupConstraints() {
        contentStackView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
        
        feedbackView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(Constants.FeedbackView.heightMultiplerScale)
            make.top.equalToSuperview().inset(Constants.FeedbackView.top)
            make.leading.trailing.equalToSuperview()
        }

        loading.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.top.leading.bottom.trailing.equalToSuperview()
        }
    }
    
    // MARK: - Helpers
    
    func selectedCharactersTableViewIndex() -> Int? {
        return charactersTableView.indexPathForSelectedRow?.row
    }
    
    func indexFor(cell: CharacterTableViewCell) -> Int? {
        return charactersTableView.indexPath(for: cell)?.row
    }
    
    func setupFrom(viewModel: ListCharactersModels.ViewModel) {
        switch viewModel.viewState {
        case .charactersListPagingLoading:
            break
        case .fetchingCharactersLoading:
            setupFetchingCharactersLoadingState()
        case .fetchingCharactersLoadingFinished:
            setupFetchingCharactersLoadingFinished()
        case .showingFeedback:
            setupShowingFeedbackState(viewModel: viewModel)
        case .showingCharactersList:
            setupShowingCharactersList()
        }
    }
    
    func setupFrom(viewModel: ListFavoritedCharactersModels.ViewModel) {
        switch viewModel.state {
        case .emptyState:
            setupShowingFeedbackState(viewModel: viewModel)
        case .showingCharactersList:
            setupShowingCharactersList()
        }
    }
    
    func setupViewDelegates(
        delegate: UITableViewDelegate,
        dataSource: UITableViewDataSource,
        searchBarDelegate: UISearchBarDelegate,
        viewDelegate: ListCharactersViewDelegate? = nil
    ) {
        charactersTableView.delegate = delegate
        charactersTableView.dataSource = dataSource
        searchBar.delegate = searchBarDelegate
        self.delegate = viewDelegate
    }
    
    func resetSearchBarText() {
        searchBar.text = .empty
        charactersTableView.setContentOffset(.zero, animated: false)
    }
    
    // MARK: - Private Methods
    
    private func charactersListReloadData() {
        charactersTableView.reloadData()
    }
    
    private func setupShowingFeedbackState(viewModel: ListCharactersModels.ViewModel) {
        guard let message = viewModel.feedbackMessage,
              let image = viewModel.feedbackImage,
              let titleButton = viewModel.feedbackTitleButton else {
            return
        }
        feedbackView.setup(feedbackImage: image, feedbackMessage: message, feedbackActionButtonTitle: titleButton)
        charactersTableView.isHidden = true
        wrapperFeedbackView.isHidden = false
    }
    
    private func setupShowingFeedbackState(viewModel: ListFavoritedCharactersModels.ViewModel) {
        guard let message = viewModel.feedbackMessage,
              let image = viewModel.feedbackImage else {
            return
        }
        feedbackView.setup(feedbackImage: image, feedbackMessage: message, feedbackActionButtonTitle: viewModel.feedbackTitleButton)
        charactersTableView.isHidden = true
        wrapperFeedbackView.isHidden = false
    }
    
    private func setupFetchingCharactersLoadingState() {
        loading.startAnimating()
    }
    
    private func setupFetchingCharactersLoadingFinished() {
        loading.stopAnimating()
    }
    
    private func setupShowingCharactersList() {
        charactersTableView.isHidden = false
        wrapperFeedbackView.isHidden = true
        charactersListReloadData()
    }
}

extension ListCharactersView: FeedbackViewDelegate {
    
    func feedbackViewDidTapFeedbackButton(_ view: FeedbackView) {
        delegate?.listCharactersViewDidTapFeedBackViewButton(self)
    }
}
