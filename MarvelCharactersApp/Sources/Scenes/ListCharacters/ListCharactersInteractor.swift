//
//  ListaCharactersInteractor.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation

protocol ListCharactersBusinessLogic {
    
    func requestedCharacter()
    func requestedFavoriteCharacter(isFavorited: Bool, indexCharacter: Int)
    func requestedSaveFavorite(characterId: Int)
    func requestedDeleteFavorite(characterId: Int)
    func requestedUpdate(character: Character)
    func resquestedResetFavoriteButtons()
    func requestedFilteredCharacters(searchText: String)
    func requestedDetailCharacter(indexCharacter: Int)
    func requestedCharactersPagination(currentIndex: Int)
}

protocol ListCharactersDataStore {
    var realmService: CharacterDataBaseServicing? {get}
}

final class ListCharactersInteractor: ListCharactersBusinessLogic, ListCharactersDataStore {
    
    private var presenter: ListCharactersPresentationLogic
    private var worker:  ListCharactersWorkable
    var realmService: CharacterDataBaseServicing?
    
    var originalResult: CharactersResult?
    private var filteredResult: CharactersResult?
    
    init(presenter: ListCharactersPresentationLogic,  worker: ListCharactersWorkable, realmService: CharacterDataBaseServicing) {
        self.presenter = presenter
        self.worker = worker
        self.realmService = realmService
    }
    
    // MARK: - ListCharactersBusinessLogic Methods
    
    func requestedCharacter() {
        worker.fetchAllFavoriteCharacter { [weak self] (favoritedCharacters) in
            if let result = self?.originalResult {
                self?.markFavoritedCharacters(result: result, currentFavoritedCharacter: favoritedCharacters)
                return
            }
            self?.presenter.presentFetchingCharacterLoadingState()
            self?.worker.fetchListCharacters { [weak self] (result) in
                DispatchQueue.main.async {
                    self?.presenter.removeFetchingCharacterLoadingState()
                    self?.handleFirstFetchedCharactersRequest(result: result, currentFavoritedCharacters: favoritedCharacters)
                }
            }
        }
    }
    
    func requestedSaveFavorite(characterId: Int) {
        if let character = filteredResult?.characters.first(where: { (current) -> Bool in
            current.id == characterId
        }) {
            worker.insertFavoriteCharacter(request: ListCharactersModels.LocalDataBase.InsertRequest(character: character.convertToEntity())) { [weak self] (sucess) in
                character.isFavorited = sucess
                self?.presenter.presentFavoritedFeedBack(
                    response: ListCharactersModels.FavoritedOperationsResponse(
                        feedbackMessage: sucess ? "favorited".localizableString : "error".localizableString
                    )
                )
            }
        }
    }
    
    func requestedDeleteFavorite(characterId: Int) {
        if let character = filteredResult?.characters.first(where: { (current) -> Bool in
            current.id == characterId
        }) {
            worker.deleteFavorite(request: ListCharactersModels.LocalDataBase.DeleteRequest(id: character.id)) { [weak self] (sucess) in
                character.isFavorited = !sucess
                let feedBackMessage = sucess ? "unfavorited".localizableString : "error".localizableString
                self?.presenter.presentFavoritedFeedBack(
                    response: ListCharactersModels.FavoritedOperationsResponse(
                        feedbackMessage: feedBackMessage
                    )
                )
            }
        }
    }
    
    func requestedFavoriteCharacter(isFavorited: Bool, indexCharacter: Int) {
        let characterId = filteredResult?.characters[indexCharacter].id ?? .zero
        isFavorited ? requestedSaveFavorite(characterId: characterId) : requestedDeleteFavorite(characterId: characterId)
    }
    
    func requestedUpdate(character: Character) {
        guard let index = originalResult?.characters.firstIndex(where: { (current) -> Bool in
            current.id == character.id
        }) else { return }
        originalResult?.characters[index].isFavorited = character.isFavorited
        self.presenter.presentFetchedCharacters(response: ListCharactersModels.Response(characters: self.originalResult, error: nil))
    }
    
    func requestedFilteredCharacters(searchText: String) {
        guard let originalResult = originalResult else { return}
        if !searchText.isEmpty {
            worker.fetchAllFavoriteCharacter { [weak self] (favoritedCharacters) in
                guard let self = self else { return }
                self.filteredResult = CharactersResult(offset: originalResult.offset, limit: originalResult.limit, total: originalResult.total, characters: self.filterCharacters(byName: searchText, in: originalResult.characters))
                guard let filteredResult = self.filteredResult else { return }
                self.markFavoritedCharacters(result: filteredResult, currentFavoritedCharacter: favoritedCharacters)
            }
        } else {
            handleFilteredCharacterForEmptySearchString()
        }
    }
    
    func requestedDetailCharacter(indexCharacter: Int) {
        guard let character = filteredResult?.characters[indexCharacter] else { return }
        self.filteredResult = self.originalResult
        presenter.presentCharacterDetail(response: ListCharactersModels.DetailCharacterResponse(character: character))
    }
    
    func requestedCharactersPagination(currentIndex: Int) {
        guard let originalResult = originalResult else { return }
        guard currentIndex == originalResult.characters.count - 1 else { return }
        let nextOffset = originalResult.offset + originalResult.limit + 1
        if nextOffset < originalResult.total {
            worker.fetchAllFavoriteCharacter { [weak self] (favoritedCharacters) in
                self?.presenter.presentFetchingCharacterLoadingState()
                self?.worker.fetchListCharactersPagination(request: ListCharactersModels.PaginationRequest(offset: nextOffset), completion: { (response) in
                    DispatchQueue.main.async {
                        self?.presenter.removeFetchingCharacterLoadingState()
                        self?.handle(result: response, currentFavoritedCharacters: favoritedCharacters)
                    }
                })
            }
        }
    }
    
    func resquestedResetFavoriteButtons() {
        if let characters = originalResult?.characters {
            for current in characters {
                current.isFavorited = false
            }
        }
    }
    
    // MARK: - Private Methods
    
    private func handle(result: Result<ListCharactersModels.Response, NetworkError>, currentFavoritedCharacters: [CharacterEntity]) {
        switch result {
        case let .success(charactersResponse):
            updateResult(newResult: charactersResponse.characters)
            guard let originalResult = originalResult else { return }
            markFavoritedCharacters(result: originalResult, currentFavoritedCharacter: currentFavoritedCharacters)
        case let .failure(error):
            presenter.presentFetchedCharacters(response: ListCharactersModels.Response(characters: nil, error: error))
        }
    }
    
    private func handleFirstFetchedCharactersRequest(result: Result<ListCharactersModels.Response, NetworkError>, currentFavoritedCharacters: [CharacterEntity]) {
        switch result {
        case let .success(response):
            self.originalResult = response.characters
            self.filteredResult = response.characters
            guard let result = response.characters else { return }
            self.markFavoritedCharacters(result: result, currentFavoritedCharacter: currentFavoritedCharacters)
        case let .failure(error):
            self.presenter.presentFetchedCharacters(response: ListCharactersModels.Response(characters: nil, error: error))
        }
    }
    
    private func checkFavorited(character: Character, completion: @escaping (Bool) -> Void) {
        worker.fetchFavoriteCharacter(request: ListCharactersModels.LocalDataBase.FetchFavoriteRequest(id: character.id)) { (entity) in
            completion(entity != nil)
        }
    }
    
    private func markFavoritedCharacters(result: CharactersResult, currentFavoritedCharacter: [CharacterEntity]) {
        currentFavoritedCharacter.forEach { (favorited) in
            if let cara = result.characters.first(where: { (current) -> Bool in
                current.id == favorited.id
            }) {
                cara.isFavorited = true
            }
        }
        self.presenter.presentFetchedCharacters(response: ListCharactersModels.Response(characters: result, error: nil))
    }
    
    private func filterCharacters(byName: String, in displayedCharacters: [Character]) -> [Character] {
        return displayedCharacters.filter({ (current) -> Bool in
            current.name.lowercased().contains(byName.lowercased())
        })
    }
    
    private func updateResult(newResult: CharactersResult?) {
        guard let newResult = newResult, let originalResult = originalResult else { return }
        let newCharactersList = originalResult.characters + newResult.characters
        let finalResult = CharactersResult(offset: newResult.offset, limit: newResult.limit, total: newResult.total, characters: newCharactersList)
        self.originalResult = finalResult
        self.filteredResult = finalResult
    }
    
    private func handleFilteredCharacterForEmptySearchString() {
        filteredResult = originalResult
        requestedCharacter()
    }
}
