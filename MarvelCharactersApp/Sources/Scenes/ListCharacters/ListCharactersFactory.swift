//
//  ListCharactersFactory.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

final class ListCharactersFactory {
    
    func setupListCharacters(network: NetworkRequesting) -> UIViewController {
        let viewController =  ListCharactersViewController()
        let realmService = CharacterRealmWorker()
        let worker = ListCharactersWorker(networkRequestor: network, realmService: realmService)
        let presenter = ListCharactersPresenter(viewController: viewController)
        let interactor = ListCharactersInteractor(presenter: presenter, worker: worker, realmService: realmService)
        let router = ListsCharactersRouter(viewController: viewController, dataStore: interactor)
        viewController.setup(interactor: interactor, router: router)
        return viewController
    }
}
