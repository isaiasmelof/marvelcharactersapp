//
//  ListCharactersPresenter.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 14/08/21.
//

import Foundation
import UIKit

protocol ListCharactersPresentationLogic {
    
    func presentFetchedCharacters(response: ListCharactersModels.Response)
    func presentFavoritedFeedBack(response: ListCharactersModels.FavoritedOperationsResponse)
    func presentFetchingCharacterLoadingState()
    func presentFilteredCharacters(response: ListCharactersModels.Response)
    func presentCharacterDetail(response: ListCharactersModels.DetailCharacterResponse)
    func removeFetchingCharacterLoadingState()
}

final class ListCharactersPresenter: ListCharactersPresentationLogic {
    
    private weak var viewController: ListCharactersDisplayLogic?
    
    init(viewController: ListCharactersDisplayLogic) {
        self.viewController = viewController
    }
    
    func presentFetchedCharacters(response: ListCharactersModels.Response) {
        viewController?.displayListCharacters(viewModel: handle(response: response))
    }
    
    func presentFetchingCharacterLoadingState() {
        let viewModel = ListCharactersModels.ViewModel(viewState: .fetchingCharactersLoading)
        viewController?.displayListCharacters(viewModel: viewModel)
    }
    
    func removeFetchingCharacterLoadingState() {
        let viewModel = ListCharactersModels.ViewModel(viewState: .fetchingCharactersLoadingFinished)
        viewController?.displayListCharacters(viewModel: viewModel)
    }
    
    func presentFavoritedFeedBack(response: ListCharactersModels.FavoritedOperationsResponse) {
        viewController?.displayFavoriteOperationsFeedback(viewModel: ListCharactersModels.FavoritedOperationsViewModel(feedbackMessage: response.feedbackMessage))
    }
    
    func presentFilteredCharacters(response: ListCharactersModels.Response) {
        viewController?.displayListCharacters(viewModel: handle(response: response))
    }
    
    // MARK: - HELPERS
    
    private func handle(response: ListCharactersModels.Response) -> ListCharactersModels.ViewModel  {
      
        guard let result = response.characters, response.error == nil  else {
            return ListCharactersModels.ViewModel.init(
                feedbackImage: .networkErrorPlaceholder,
                feedbackMessage: "feedback_network_failure_message".localizableString,
                feedbackTitleButton: "try_again".localizableString,
                viewState: .showingFeedback
            )
        }
        
        return ListCharactersModels.ViewModel.init(
            offset: result.offset,
            limit: result.limit,
            total: result.total,
            characters: result.characters,
            viewState: .showingCharactersList
        )
    }
    
    func presentCharacterDetail(response: ListCharactersModels.DetailCharacterResponse) {
        viewController?.displayCharacterDetails(viewModel: ListCharactersModels.DetailCharacterViewModel(character: response.character))
    }
    
}
