//
//  CharacterDetailPresenter.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation


protocol CharacterDetailPresentationLogic {
    func present(response: CharacterDetailModels.Response)
    func presentFavoritedFeedBack(response: CharacterDetailModels.FavoriteLocalDatabaseOperationResponse)
}

final class CharacterDetailPresenter: CharacterDetailPresentationLogic {
    
    // MARK: - Private Properties
    
    private weak var viewController: CharacterDetailDisplayLogic?
    
    // MARK: - Initializer
    
    init(viewController: CharacterDetailDisplayLogic) {
        self.viewController = viewController
    }
    
    // MARK: - CharacterDetailPresentationLogic Methods
    
    func present(response: CharacterDetailModels.Response) {
        viewController?.displayCharacter(viewModel: CharacterDetailModels.ViewModel(character: response.character, isReadOnlyMode: response.isReadOnlyMode))
    }
    
    func presentFavoritedFeedBack(response: CharacterDetailModels.FavoriteLocalDatabaseOperationResponse) {
        viewController?.displayFavoriteOperationsFeedback(viewModel: CharacterDetailModels.FavoritedOperationsViewModel(feedbackMessage: response.feedbackMessage))
    }
}
