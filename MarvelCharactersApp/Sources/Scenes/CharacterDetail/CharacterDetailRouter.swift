//
//  CharacterDetailRouter.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation


protocol CharacterDetailRoutingLogic {
    
}

protocol CharacterDetailDataPassing {
    
    var dataStore: CharacterDetailDataStore? { get set }
}


final class CharacterDetailRouter: NSObject, CharacterDetailRoutingLogic, CharacterDetailDataPassing {
    
    var dataStore: CharacterDetailDataStore?
    private weak var viewController: CharacterDetailViewController?
    
    init(viewController: CharacterDetailViewController, dataStore: CharacterDetailDataStore) {
        self.viewController = viewController
        self.dataStore = dataStore
    }
}
