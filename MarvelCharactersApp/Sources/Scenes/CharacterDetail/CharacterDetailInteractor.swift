//
//  CharacterDetailInteractor.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation

protocol CharacterDetailBusinessLogic {
    
    func requestedCharacter()
    func requestedUpdateCharacter(isFavorited: Bool)
}

protocol CharacterDetailDataStore
{
  var character: Character { get set }
}

final class CharacterDetailInteractor: CharacterDetailBusinessLogic, CharacterDetailDataStore {
   
    // MARK: Internal Properties
    
    var character: Character
    
    // MARK: Private Properties
    
    private var presenter: CharacterDetailPresentationLogic
    private var worker: CharacterDetailWorkable
    private var isReadOnlyMode: Bool
    
    // MARK: Initializer
    
    init(presenter: CharacterDetailPresentationLogic, character: Character, isReadOnlyMode:Bool, worker: CharacterDetailWorkable) {
        self.character = character
        self.presenter = presenter
        self.worker = worker
        self.isReadOnlyMode = isReadOnlyMode
    }
    
    // MARK: CharacterDetailBusinessLogic Methods
    
    func requestedCharacter() {
        worker.fetchFavoriteCharacter(request: ListCharactersModels.LocalDataBase.FetchFavoriteRequest(id: character.id)) { [weak self] (entity) in
            guard let self = self else { return }
            if let entity = entity  {
                self.presenter.present(response: CharacterDetailModels.Response(character: Character(character: entity.convertToObject(), isFavorited: true), isReadOnlyMode: self.isReadOnlyMode))
            } else {
                self.character.isFavorited = false
                self.presenter.present(response: CharacterDetailModels.Response(character: Character(character: self.character, isFavorited: false), isReadOnlyMode: self.isReadOnlyMode))
            }
        }
    }
    
    func requestedUpdateCharacter(isFavorited: Bool) {
        isFavorited ?
            worker.insertFavoriteCharacter(request: ListCharactersModels.LocalDataBase.InsertRequest(character: character.convertToEntity())) { [weak self] (success) in
                if success { self?.character.isFavorited = true }
                self?.presenter.presentFavoritedFeedBack(
                        response: CharacterDetailModels.FavoriteLocalDatabaseOperationResponse(
                            feedbackMessage: success ? "favorited".localizableString : "error".localizableString
                        )
                    )
            }:
            worker.deleteFavorite(request: ListCharactersModels.LocalDataBase.DeleteRequest(id: character.id)) { [weak self] (success) in
                if success { self?.character.isFavorited = false }
                self?.presenter.presentFavoritedFeedBack(
                        response: CharacterDetailModels.FavoriteLocalDatabaseOperationResponse(
                            feedbackMessage: success ? "unfavorited".localizableString : "error".localizableString
                        )
                    )
            }
    }
}
