//
//  CharacterDetailViewController.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation

protocol CharacterDetailDisplayLogic: AnyObject {
    
    func displayCharacter(viewModel: CharacterDetailModels.ViewModel)
    func displayFavoriteOperationsFeedback(viewModel:  CharacterDetailModels.FavoritedOperationsViewModel)
}

final class CharacterDetailViewController: BaseViewController<CharacterDetailView>, CharacterDetailDisplayLogic {
   
    // MARK: - Internal Properties
    
    var router: (CharacterDetailRoutingLogic & CharacterDetailDataPassing)?
    
    // MARK: - Private Properties
    
    private var interactor: CharacterDetailBusinessLogic?
    
    // MARK: - LifeCycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        interactor?.requestedCharacter()
    }
    
    // MARK: - Setup
    
    func setup(interactor: CharacterDetailBusinessLogic?, router: (CharacterDetailRoutingLogic & CharacterDetailDataPassing)?) {
        self.interactor = interactor
        self.router = router
        associatedView.delegate = self
    }
    
    // MARK: - CharacterDetailDisplayLogic
    
    func displayCharacter(viewModel: CharacterDetailModels.ViewModel) {
        associatedView.setupFrom(viewModel: viewModel)
    }
    
    func displayFavoriteOperationsFeedback(viewModel: CharacterDetailModels.FavoritedOperationsViewModel) {
        showToast(message: viewModel.feedbackMessage, font: viewModel.font)
    }
    
}

extension CharacterDetailViewController: CharacterDetailViewDelegate {
    
    func didTappedFavoriteButton(_ view: CharacterDetailView, isFavorited: Bool) {
        interactor?.requestedUpdateCharacter(isFavorited: isFavorited)
    }
}
