//
//  CharacterDetailFactory.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation
import UIKit

final class CharacterDetailFactory {
    
    func setupCharacterDetail(character: Character, realmService: CharacterDataBaseServicing?, isReadOnlyMode: Bool = false) -> CharacterDetailViewController {
        let viewController =  CharacterDetailViewController()
        let presenter = CharacterDetailPresenter(viewController: viewController)
        let interactor = CharacterDetailInteractor(
            presenter: presenter,
            character: character,
            isReadOnlyMode: isReadOnlyMode,
            worker: CharacterDetailWorker(realmService: realmService
            )
        )
        let router = CharacterDetailRouter(viewController: viewController, dataStore: interactor)
        viewController.setup(interactor: interactor, router: router)
        return viewController
    }
}
