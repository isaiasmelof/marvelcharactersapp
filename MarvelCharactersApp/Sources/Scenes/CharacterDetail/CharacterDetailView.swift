//
//  CharacterDetailView.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 16/08/21.
//

import Foundation
import UIKit

protocol CharacterDetailViewDelegate: AnyObject {
    
    func didTappedFavoriteButton(_ view: CharacterDetailView, isFavorited: Bool)
}

final class CharacterDetailView: UIView, CodableView {
    
    // MARK: - Constants
    
    enum Constants {

        struct CharacterImageView {
            static let size: CGFloat = 250
            static let cornerRadius: CGFloat = size / 2
        }
        
        struct HorizontalCharacterNameAndFavoriteButtonStackView {
            static let spacing: CGFloat = 12
            static let top: CGFloat = 12
        }
        
        struct FavoriteButton {
            static let size: CGFloat = 40
        }
        
        struct CharacterDescriptionLabel {
            static let top: CGFloat = 16
        }
        
        static let defaultMargin: CGFloat = 32
    }
    
    
    // MARK: - UI
    
    private lazy var horizontalCharacterNameAndFavoriteButtonStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [favoriteButton, wrapperCharacterNameLabel])
        stack.axis = .horizontal
        stack.alignment = .center
        stack.spacing = Constants.HorizontalCharacterNameAndFavoriteButtonStackView.spacing
        stack.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return stack
    }()
    
    private lazy var characterImageView: CachedImageView = {
        let imageView = CachedImageView(cornerRadius: Constants.CharacterImageView.cornerRadius, emptyImage: .characterThumbnailPlaceholder)
        imageView.shouldUseEmptyImage = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var wrapperCharacterNameLabel: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var characterNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = .zero
        return label
    }()
    
    private lazy var characterDescriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = .zero
        label.textAlignment = .justified
        return label
    }()
    
    private lazy var favoriteButton: UIButton = {
        let button = UIButton()
        button.setImage(.emptyStar, for: .normal)
        button.addTarget(self, action: #selector(didTapFavoriteButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Internal Properties
    
    weak var delegate: CharacterDetailViewDelegate?
    
    // MARK: - Initializers
    
    init(){
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - CodableView Methods
    
    func buildViews(){
        addSubview(characterImageView)
        addSubview(horizontalCharacterNameAndFavoriteButtonStackView)
        addSubview(characterDescriptionLabel)
        wrapperCharacterNameLabel.addSubview(characterNameLabel)
    }
    
    func setupComponents(){
        backgroundColor = .systemBackground
    }
    
    func setupConstraints(){
        
        characterImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(Constants.CharacterImageView.size)
            make.top.equalToSuperview().inset(Constants.defaultMargin)
            make.centerX.equalToSuperview()
        }
        
        horizontalCharacterNameAndFavoriteButtonStackView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(Constants.defaultMargin)
            make.top.equalTo(characterImageView.snp.bottom).offset(Constants.HorizontalCharacterNameAndFavoriteButtonStackView.top)
            make.height.equalTo(Constants.FavoriteButton.size)
        }
        
        favoriteButton.snp.makeConstraints { (make) in
            make.height.width.equalTo(Constants.FavoriteButton.size)
        }
        
        characterNameLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        characterDescriptionLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(horizontalCharacterNameAndFavoriteButtonStackView)
            make.top.equalTo(horizontalCharacterNameAndFavoriteButtonStackView.snp.bottom).offset(Constants.CharacterDescriptionLabel.top)
        }
    }
    
    // MARK: - Setup
    
    func setupFrom(viewModel: CharacterDetailModels.ViewModel) {
        characterImageView.loadImage(urlString: viewModel.character.thumbnail.urlToDownload)
        characterNameLabel.text = viewModel.character.name
        if !viewModel.character.name.isEmpty {
            characterDescriptionLabel.text = viewModel.character.description
        }
        let favoriteButtonImage: UIImage = viewModel.character.isFavorited  ?? false ? .filledStar : .emptyStar
        favoriteButton.setImage(favoriteButtonImage, for: .normal)
        favoriteButton.isHidden = viewModel.isReadOnlyMode
    }
    
    // MARK: - Private Methods
    
    @objc
    private func didTapFavoriteButton() {
        let newButtonImage: UIImage = favoriteButton.imageView?.image == UIImage.emptyStar ? .filledStar : .emptyStar
        delegate?.didTappedFavoriteButton(self, isFavorited: favoriteButton.imageView?.image != .filledStar)
        favoriteButton.setImage(newButtonImage, for: .normal)
    }
}
