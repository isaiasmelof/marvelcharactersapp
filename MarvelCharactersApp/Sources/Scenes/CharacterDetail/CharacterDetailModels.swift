//
//  CharacterDetailModels.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation
import UIKit

struct CharacterDetailModels {
    
    struct Response {
        let character: Character
        let isReadOnlyMode: Bool
    }
    
    struct FavoriteLocalDatabaseOperationResponse {
        let feedbackMessage: String
    }
    
    struct ViewModel {
        let character: Character
        let isReadOnlyMode: Bool
    }
    
    struct FavoritedOperationsViewModel {
        let feedbackMessage: String
        let font: UIFont = .boldSystemFont(ofSize: 12)
    }
}
