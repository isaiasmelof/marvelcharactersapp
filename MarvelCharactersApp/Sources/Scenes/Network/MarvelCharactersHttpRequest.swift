//
//  MarvelCharactersHttpRequest.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

enum MarvelCharactersHttpRequest: NetworkRequestBuilding {
    
    case characters(CharactersRequestContent)
    case characterDetail(id: String)
    
    var basePath: String {
        return "/v1/public"
    }
    
    var path: String {
        switch self {
        case .characters:
            return "\(basePath)/characters"
        case let .characterDetail(id):
            return "\(basePath)characters/\(id)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var parameters: NetworkRequestParameters? {
        switch self {
        case let .characters(request):
            return .queryItems(items: request.toKeyValueArray())
        default:
            return nil
        }
    }
    
    var headers: [String : Any]? {
        return nil
    }
    
}
