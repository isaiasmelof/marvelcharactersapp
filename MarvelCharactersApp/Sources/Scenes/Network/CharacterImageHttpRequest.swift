//
//  CharacterImageHttpRequest.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 16/08/21.
//

import Foundation

enum CharacterImageHttpRequest: NetworkRequestBuilding {
    
    case image(String)
    
    var baseUrl: URL? {
        switch self {
        case let .image(urlString):
            return URL(string: urlString)
        }
    }
    
    var path: String {
        switch self {
        case let .image(urlString):
            guard let url = URL(string: urlString) else { return "" }
            return url.path
        }
    }
    
    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var parameters: NetworkRequestParameters? {
        return nil
    }
    
    var headers: [String : Any]? {
        return nil
    }
    
}
