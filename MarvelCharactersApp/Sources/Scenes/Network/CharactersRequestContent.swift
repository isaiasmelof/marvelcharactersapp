//
//  CharactersRequest.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation

struct CharactersRequestContent: Encodable {
    
    let ts = MarvelAPIAuthModel.timestamp
    let apikey =  MarvelAPIAuthModel.publicKey
    let hash = MarvelAPIAuthModel.hash
    var offset: String
    var limit: String
    
    init(offset: String = 0.toString(), limit: String = 50.toString()) {
        self.offset = offset
        self.limit = limit
    }
}
