//
//  ListFavoritedCharactersFactory.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation
import UIKit

final class ListFavoritedCharactersFactory {
    
    func setupListFavoritedCharacters() -> UIViewController {
        let viewController =  ListFavoritedCharactersViewController()
        let presenter = ListFavoritedCharactersPresenter(viewController: viewController)
        let realmService = CharacterRealmWorker()
        let worker = ListFavoritedCharactersWorker(realmService: realmService)
        let interactor = ListFavoritedCharactersInteractor(presenter: presenter, worker: worker, realmService: realmService)
        let router = ListFavoritedCharactersRouter(viewController: viewController, dataStore: interactor)
        viewController.setup(interactor: interactor, router: router)
        return viewController
    }
}
