//
//  ListFavoritedCharactersWorker.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation


protocol ListFavoritedCharactersWorkable {
    
    func fetchAllFavoritedCharacter(completion: @escaping ([CharacterEntity]) -> Void)
}


final class  ListFavoritedCharactersWorker: ListFavoritedCharactersWorkable {
    
    // MARK: - Private properties
    
    private var realmService: CharacterDataBaseServicing?
    
    // MARK: - Initializer
    
    init(realmService: CharacterDataBaseServicing?) {
        self.realmService = realmService
    }
    
    // MARK: - ListFavoritedCharactersWorkable Methods
    
    func fetchAllFavoritedCharacter(completion: @escaping ([CharacterEntity]) -> Void) {
        realmService?.fetchFavoritesCharacters(completion: { (characters) in
            completion(characters)
        })
    }
}
