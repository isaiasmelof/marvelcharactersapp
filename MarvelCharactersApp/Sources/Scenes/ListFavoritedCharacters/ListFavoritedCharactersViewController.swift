//
//  ListFavoritedCharactersViewController.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation
import UIKit

protocol ListFavoritedCharactersDisplayLogic: AnyObject {
    
    func displayFavoritedCharacter(viewModel: ListFavoritedCharactersModels.ViewModel)
    func displayCharacterDetails(viewModel:  ListFavoritedCharactersModels.DetailCharacterViewModel)
}



final class ListFavoritedCharactersViewController: BaseViewController<ListCharactersView>, ListFavoritedCharactersDisplayLogic {
    
    // MARK: - Private Properties
    private var interactor: ListFavoritedCharactersBusinessLogic?
    private var viewModel: ListFavoritedCharactersModels.ViewModel?
    
    
    // MARK: - Internal Properties
    
    var router: ListFavoritedCharactersRoutingLogic?
    
    // MARK: - Setup
    
    func setup(interactor: ListFavoritedCharactersBusinessLogic?, router: ListFavoritedCharactersRoutingLogic?) {
        self.interactor = interactor
        self.router = router
        associatedView.setupViewDelegates(delegate: self, dataSource: self, searchBarDelegate: self)
        title = "favorites".localizableString
    }
    
    // MARK: - Lifecycle Methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.requestedFavoritedCharacters()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        associatedView.resetSearchBarText()
    }

    // MARK: - ListFavoritedCharactersDisplayLogic Methods
    
    func displayFavoritedCharacter(viewModel: ListFavoritedCharactersModels.ViewModel) {
        self.viewModel = viewModel
        DispatchQueue.main.async { [weak self] in
            self?.associatedView.setupFrom(viewModel: viewModel)
        }
    }
    
    func displayCharacterDetails(viewModel:  ListFavoritedCharactersModels.DetailCharacterViewModel) {
        router?.routeToDetailCharacter(character: viewModel.character, isReadOnlyMode: viewModel.isReadOnlyMode)
    }
}

extension ListFavoritedCharactersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.characters.count ?? .zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reusableCell = tableView.dequeueReusableCell(withIdentifier: CharacterTableViewCell.className, for: indexPath)
        
        guard let cell = reusableCell as? CharacterTableViewCell,
              let character =  viewModel?.characters[safe: indexPath.row] else {
            return CharacterTableViewCell()
        }
        
        cell.setup(
            viewModel: ListCharactersModels.ViewModel.CharacterCellViewModel(
                thumbnailUrl: character.thumbnail.urlToDownload,
                characterName: character.name,
                isFavoriteButtonHidden: true)
        )
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor?.requestedFavoritedCharacterDetail(indexCharacter: indexPath.row)
    }

}

extension ListFavoritedCharactersViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        interactor?.requestedFilteredCharacters(searchText: searchText)
    }
}
