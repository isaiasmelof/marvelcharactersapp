//
//  ListFavoritedCharactersInteractor.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation

protocol ListFavoritedCharactersBusinessLogic {
    
    func requestedFavoritedCharacters()
    func requestedFavoritedCharacterDetail(indexCharacter: Int)
    func requestedFilteredCharacters(searchText: String)
}

final class ListFavoritedCharactersInteractor: ListFavoritedCharactersBusinessLogic, ListCharactersDataStore {
    
    // MARK: - Internal Properties
    
    var realmService: CharacterDataBaseServicing?
    
    // MARK: - Private Properties
    
    private var presenter: ListFavoritedCharactersPresentationLogic
    private var worker: ListFavoritedCharactersWorkable
    private var favoritedCharacters: [Character]?
    private var filteredCharacters: [Character]?
    
    // MARK: - Initializer
    
    init(presenter: ListFavoritedCharactersPresentationLogic, worker: ListFavoritedCharactersWorkable, realmService: CharacterDataBaseServicing?) {
        self.presenter = presenter
        self.worker = worker
        self.realmService = realmService
    }
    
    // MARK: - ListFavoritedCharactersBusinessLogic Methods
    
    func requestedFavoritedCharacters() {
        worker.fetchAllFavoritedCharacter { [weak self] (characters) in
            guard let characters = self?.convertArrayEntityToArrayDisplay(characters: characters) else { return }
            self?.favoritedCharacters = characters
            self?.filteredCharacters = self?.favoritedCharacters
            self?.presenter.presentFavoritedCharacters(response: ListFavoritedCharactersModels.Response(favoritedCharacters: characters))
        }
    }
    
    func requestedFilteredCharacters(searchText: String) {
        if searchText.isEmpty {
            requestedFavoritedCharacters()
        } else {
            self.filteredCharacters = favoritedCharacters?.filter({ (current) -> Bool in
                current.name.contains(searchText)
            }) ?? []
            presenter.presentFavoritedCharacters(response: ListFavoritedCharactersModels.Response(isFilterFlow: true, favoritedCharacters: filteredCharacters ?? []))
        }
    }
    
    func requestedFavoritedCharacterDetail(indexCharacter: Int) {
        guard let character = filteredCharacters?[indexCharacter] else { return }
        presenter.presentCharacterDetail(response: ListFavoritedCharactersModels.DetailCharacterResponse(character: character))
    }
    
    // MARK: - Private methods
    
    private func convertArrayEntityToArrayDisplay(characters: [CharacterEntity]) -> [Character] {
        var newArray = [Character]()
        characters.forEach { (current) in
            newArray.append(current.convertToObject())
        }
        return newArray
    }
    
}
