//
//  ListFavoritedCharactersPresenter.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation

protocol ListFavoritedCharactersPresentationLogic {
    
    func presentFavoritedCharacters(response: ListFavoritedCharactersModels.Response)
    func presentCharacterDetail(response: ListFavoritedCharactersModels.DetailCharacterResponse)
}

final class ListFavoritedCharactersPresenter: ListFavoritedCharactersPresentationLogic {
    
    // MARK: - Private properties
    
    private weak var viewController: ListFavoritedCharactersDisplayLogic?
    
    // MARK: - Initializer
    
    init(viewController: ListFavoritedCharactersDisplayLogic) {
        self.viewController = viewController
    }
    
    // MARK: - ListFavoritedCharactersPresentationLogic
    
    func presentFavoritedCharacters(response: ListFavoritedCharactersModels.Response) {
        let state: ListFavoritedCharactersModels.ViewModel.State =
            (response.favoritedCharacters.isEmpty && !response.isFilterFlow) ? .emptyState: .showingCharactersList
        
        if state == .showingCharactersList {
            viewController?.displayFavoritedCharacter(viewModel: ListFavoritedCharactersModels.ViewModel(characters: response.favoritedCharacters, state: state))
        } else {
            if !response.isFilterFlow {
                viewController?.displayFavoritedCharacter(viewModel: ListFavoritedCharactersModels.ViewModel(characters: [], state: .emptyState, feedbackImage: .emptyContentPlaceholder, feedbackMessage: "favorited_empty_state_message".localizableString))
                
            }
        }
        
    }
    
    func presentCharacterDetail(response: ListFavoritedCharactersModels.DetailCharacterResponse) {
        viewController?.displayCharacterDetails(viewModel: ListFavoritedCharactersModels.DetailCharacterViewModel(character: response.character))
    }
    
}
