//
//  ListFavoritedCharactersModels.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation
import UIKit

struct ListFavoritedCharactersModels {
    
    struct Response {
        let isFilterFlow: Bool
        let favoritedCharacters: [Character]
        
        init(isFilterFlow: Bool = false, favoritedCharacters: [Character]) {
            self.isFilterFlow = isFilterFlow
            self.favoritedCharacters = favoritedCharacters
        }
    }
    
    struct DetailCharacterResponse {
        let character: Character
    }
    
    struct DetailCharacterViewModel {
        let character: Character
        let isReadOnlyMode = true
    }
    
    struct ViewModel {
        
        enum State {
            case showingCharactersList
            case emptyState
        }
        
        let characters: [Character]
        let state: State
        let feedbackImage: UIImage?
        let feedbackMessage: String?
        let feedbackTitleButton: String?
        
        init(
            characters: [Character],
            state: ListFavoritedCharactersModels.ViewModel.State,
            feedbackImage: UIImage? = nil,
            feedbackMessage: String? = nil,
            feedbackTitleButton: String? = nil
        ) {
            self.characters = characters
            self.state = state
            self.feedbackImage = feedbackImage
            self.feedbackMessage = feedbackMessage
            self.feedbackTitleButton = feedbackTitleButton
        }
    }
}
