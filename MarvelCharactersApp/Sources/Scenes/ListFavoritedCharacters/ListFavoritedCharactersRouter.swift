//
//  ListFavoritedCharactersRouter.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 17/08/21.
//

import Foundation

protocol ListFavoritedCharactersRoutingLogic {
    
    func routeToDetailCharacter(character: Character, isReadOnlyMode: Bool)
}

protocol ListFavoritedCharactersDataPassing {
    var dataStore: ListCharactersDataStore? { get }
}

final class ListFavoritedCharactersRouter: ListFavoritedCharactersRoutingLogic, ListFavoritedCharactersDataPassing {
    
    var dataStore: ListCharactersDataStore?
    private weak var viewController: ListFavoritedCharactersViewController?
    
    init(viewController: ListFavoritedCharactersViewController,  dataStore: ListCharactersDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }
    
    func routeToDetailCharacter(character: Character, isReadOnlyMode: Bool) {
        let nextViewController = CharacterDetailFactory().setupCharacterDetail(character: character, realmService: dataStore?.realmService, isReadOnlyMode: isReadOnlyMode)
        viewController?.navigationController?.pushViewController(nextViewController, animated: true)
    }
}
