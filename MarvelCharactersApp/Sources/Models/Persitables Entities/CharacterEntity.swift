//
//  CharacterEntity.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 15/08/21.
//

import Foundation
import RealmSwift

final class CharacterEntity: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var characterDescription: String = ""
    @objc dynamic var thumbnailUrl: String = ""
    @objc dynamic var thumbnailExtension: String = ""
    
    func convertToObject() -> Character {
        return Character(
            id: id,
            name: name,
            description: characterDescription,
            thumbnail: Thumbnail(path: thumbnailUrl, thumbnailExtension: thumbnailExtension),
            isFavorited: true
        )
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
