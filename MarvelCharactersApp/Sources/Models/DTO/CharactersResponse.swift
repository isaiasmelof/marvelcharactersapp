//
//  CharactersResponse.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 13/08/21.
//

import Foundation
import UIKit

class CharactersResponse: Codable {
    let data: CharactersResult
    
    init(data: CharactersResult) {
        self.data = data
    }
}

class CharactersResult: Codable {
    
    let offset: Int
    let limit: Int
    let total: Int
    let characters: [Character]
    
    init(offset: Int, limit: Int, total: Int, characters: [Character]) {
        self.offset = offset
        self.limit = limit
        self.total = total
        self.characters = characters
    }
    
    enum CodingKeys: String, CodingKey {
        case offset, limit, total
        case characters = "results"
    }
}

class Character: Codable {
    let id: Int
    let name: String
    let description: String
    let thumbnail: Thumbnail
    var isFavorited: Bool?
    
    init(character: Character, isFavorited: Bool) {
        self.id = character.id
        self.name = character.name
        self.description = character.description
        self.thumbnail = character.thumbnail
        self.isFavorited = isFavorited
    }
    
    init(
        id: Int,
        name: String,
        description: String,
        thumbnail: Thumbnail,
        isFavorited: Bool? = false
    ) {
        self.id = id
        self.name = name
        self.description = description
        self.thumbnail = thumbnail
        self.isFavorited = isFavorited
    }
    
    func convertToEntity() -> CharacterEntity {
        let entity = CharacterEntity()
        entity.id = id
        entity.name = name
        entity.characterDescription = description
        entity.thumbnailUrl = thumbnail.path
        entity.thumbnailExtension = thumbnail.thumbnailExtension
        return entity
    }
}

class Thumbnail: Codable {
    
    let path: String
    let thumbnailExtension: String
    
    var urlToDownload: String {
        return "\(path).\(thumbnailExtension)"
    }

    init(path: String, thumbnailExtension: String) {
        self.path = path
        self.thumbnailExtension = thumbnailExtension
    }
    
    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}
