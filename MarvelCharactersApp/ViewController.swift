//
//  ViewController.swift
//  MarvelCharactersApp
//
//  Created by Isaias Fernandes on 12/08/21.
//

import UIKit

class ViewController: UIViewController {

    let requestor = RequestorFactory().buildRequestor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .gray
    }

}

